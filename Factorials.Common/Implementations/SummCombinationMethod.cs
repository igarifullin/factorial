﻿using System.Collections.Generic;
using System.Numerics;

namespace Factorials.Common.Implementations
{
    public class SummCombinationMethod : BaseMethod<int, BigInteger>, IFactorialMethod
    {
        public SummCombinationMethod(bool withCache) : base(withCache: withCache)
        {
            _pascalTriangle = new Dictionary<int, Dictionary<int, BigInteger>>();
        }

        protected override BigInteger CalculateWithCache(int n)
        {
            if (n == 0)
            {
                return 1;
            }

            BigInteger summand, summa = 0;
            for (var k = 1; k <= n; k++)
            {
                summand = BigInteger.Pow(value: -1, exponent: n + k);
                summand *= CalculateCombination(k: k, n: n);
                summand *= BigInteger.Pow(value: k, exponent: n);
                summa += summand;
            }
            return summa;
        }

        protected override BigInteger CalculateWithoutCache(int n)
        {
            if (n == 0)
            {
                return 1;
            }

            BigInteger summand, summa = 0;
            for (var k = 1; k <= n; k++)
            {
                summand = BigInteger.Pow(value: -1, exponent: n + k);
                summand *= CalculateCombinationWithoutCache(k: k, n: n);
                summand *= BigInteger.Pow(value: k, exponent: n);
                summa += summand;
            }
            return summa;
        }

        protected override bool IsInvalidArgument(int n)
        {
            return n < 0;
        }

        private BigInteger CalculateCombinationWithoutCache(int k, int n)
        {
            if (k == n || k == 0)
            {
                return 1;
            }
            
            return CalculateCombinationWithoutCache(k: k - 1, n: n - 1) 
                + CalculateCombinationWithoutCache(k: k, n: n - 1);
        }

        private BigInteger CalculateCombination(int k, int n)
        {
            if (k == n || k == 0)
            {
                return 1;
            }

            BigInteger result;
            Dictionary<int, BigInteger> comb;
            if (_pascalTriangle.TryGetValue(key: n, value: out comb))
            {
                if (_pascalTriangle[n].TryGetValue(key: k, value: out result))
                {
                    return result;
                }
            }
            else
            {
                comb = new Dictionary<int, BigInteger>();
                _pascalTriangle.Add(key: n, value: comb);
            }
            result = CalculateCombination(k: k - 1, n: n - 1) + CalculateCombination(k: k, n: n - 1);
            _pascalTriangle[n].Add(key: k, value: result);

            return result;
        }

        private readonly Dictionary<int, Dictionary<int, BigInteger>> _pascalTriangle;
    }
}
