﻿using System;
using System.Collections.Generic;
using Factorials.Common.Implementations;
using NUnit.Framework;

namespace Factorials.Common.Tests
{
    /// <summary>
    /// Тесты проверки вычислений метода имени Льва Ганджумовича Амбарцумова
    /// </summary>
    public class SummCombinationMethodTests : BaseTests
    {
        public override IFactorialMethod GetMethod()
        {
            return new SummCombinationMethod(withCache: false);
        }
    }
}
