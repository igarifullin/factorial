﻿using System;
using System.Collections.Generic;
using System.Numerics;
using Factorials.Common.Implementations;
using NUnit.Framework;
using NUnit.Framework.Compatibility;

namespace Factorials.Common.Tests.Perfomance
{
    /// <summary>
    /// Тесты с большими данными
    /// </summary>
    public class RecursiveMethodPerfomanceTests
    {
        [Test, TestCaseSource(nameof(TestCaseDatasWithoutCache))]
        public void WithoutCache(int n, BigInteger expectingResult, BigInteger expectingTime)
        {
            var method = GetMethod(withCache: false);
            var sw = new Stopwatch();

            sw.Start();
            var result = method.Calculate(n);
            sw.Stop();
            var timeResult = sw.ElapsedMilliseconds;

            Console.WriteLine("Result = {0}, Time = {1}.", result, timeResult);
            Assert.That(result, Is.EqualTo(expectingResult));
            Assert.That(timeResult < expectingTime, Is.True,
                message: $"Expected: less than or equal to {expectingTime}. But was: {timeResult}");
        }

        [Test, TestCaseSource(nameof(TestCaseDatasWithCache))]
        public void WithCache(int n, BigInteger expectingResult, BigInteger expectingTime)
        {
            var method = GetMethod(withCache: true);
            var sw = new Stopwatch();

            sw.Start();
            var result = method.Calculate(n);
            sw.Stop();
            var timeResult = sw.ElapsedMilliseconds;

            Console.WriteLine("Result = {0}, Time = {1}.", result, timeResult);
            Assert.That(result, Is.EqualTo(expectingResult));
            Assert.That(timeResult < expectingTime, Is.True,
                message: $"Expected: less than or equal to {expectingTime}. But was: {timeResult}");
        }

        public void WithoutCache_BigData(int n, BigInteger expectingResult, BigInteger expectingTime)
        {
            var method = GetMethod(withCache: false);
            var sw = new Stopwatch();

            sw.Start();
            var result = method.Calculate(n);
            sw.Stop();
            var timeResult = sw.ElapsedMilliseconds;

            Console.WriteLine("Result = {0}, Time = {1}.", result, timeResult);
            Assert.That(result, Is.EqualTo(expectingResult));
            Assert.That(timeResult < expectingTime, Is.True,
                message: $"Expected: less than or equal to {expectingTime}. But was: {timeResult}");
        }

        [Test, TestCaseSource(nameof(TestCase_SmallDatas))]
        public void WithCachVsWithoutCache_WithoutUsing_SmallDatas(int n)
        {
            var method = GetMethod(withCache: false);
            var sw = new Stopwatch();

            sw.Start();
            var firstResult = method.Calculate(n);
            sw.Stop();
            var firstTimeResult = sw.ElapsedMilliseconds;

            sw.Reset();
            method = GetMethod(withCache: true);
            sw.Start();
            var secondResult = method.Calculate(n);
            sw.Stop();
            var secondTimeResult = sw.ElapsedMilliseconds;

            Assert.That(firstResult, Is.EqualTo(secondResult));
            Console.WriteLine($"First {firstTimeResult}. Second {secondTimeResult}");
            Assert.That(firstTimeResult <= secondTimeResult, Is.True);
        }

        //[Test, TestCaseSource(nameof(TestCase_BigDatas))]
        public void WithCachVsWithoutCache_WithoutUsing_BigDatas(int n)
        {
            var method = GetMethod(withCache: false);
            var sw = new Stopwatch();

            sw.Start();
            var firstResult = method.Calculate(n);
            sw.Stop();
            var firstTimeResult = sw.ElapsedMilliseconds;

            sw.Reset();

            method = GetMethod(withCache: true);

            sw.Start();
            var secondResult = method.Calculate(n);
            sw.Stop();
            var secondTimeResult = sw.ElapsedMilliseconds;

            Assert.That(firstResult, Is.EqualTo(secondResult));
            Console.WriteLine($"First {firstTimeResult}. Second {secondTimeResult}");
            //Assert.That(firstTimeResult <= secondTimeResult, Is.True);
        }

        [Test, TestCaseSource(nameof(TestCase_BigDatas))]
        public void WithCachVsWithoutCache_WithUsing_BigDatas(int n)
        {
            var method = GetMethod(withCache: false);
            var sw = new Stopwatch();

            sw.Start();
            var firstResult = method.Calculate(n);
            sw.Stop();
            var firstTimeResult = sw.ElapsedMilliseconds;

            sw.Reset();

            method = GetMethod(withCache: true);
            var m = n/2;
            m = m > 5000 ? 5000 : m;
            for (var i = 0; i < m; i++)
            {
                method.Calculate(i);
            }

            sw.Start();
            var secondResult = method.Calculate(n);
            sw.Stop();
            var secondTimeResult = sw.ElapsedMilliseconds;

            Assert.That(firstResult, Is.EqualTo(secondResult));
            Console.WriteLine($"First {firstTimeResult}. Second {secondTimeResult}");
            //Assert.That(firstTimeResult <= secondTimeResult, Is.True);
        }

        public IFactorialMethod GetMethod(bool withCache)
        {
            return new RecursiveMethod(withCache: withCache);
        }

        public static IEnumerable<TestCaseData> TestCaseDatasWithCache()
        {
            return TestCaseDatas(withCache: true);
        }

        public static IEnumerable<TestCaseData> TestCaseDatasWithoutCache()
        {
            return TestCaseDatas(withCache: false);
        }

        public static IEnumerable<TestCaseData> TestCaseDatas(bool withCache)
        {
            if (withCache)
            {
                return new List<TestCaseData>
                {
                    TestData(n: 0, expectingResult: 1, expectingTime: 8),
                    TestData(n: 1, expectingResult: 1, expectingTime: 23),
                    TestData(n: 2, expectingResult: 2, expectingTime: 4),
                    TestData(n: 3, expectingResult: 6, expectingTime: 1),
                    TestData(n: 4, expectingResult: 24, expectingTime: 5),
                    TestData(n: 5, expectingResult: 120, expectingTime: 1)
                };
            }
            return new List<TestCaseData>
            {
                TestData(n: 0, expectingResult: 1, expectingTime: 1),
                TestData(n: 1, expectingResult: 1, expectingTime: 1),
                TestData(n: 2, expectingResult: 2, expectingTime: 1),
                TestData(n: 3, expectingResult: 6, expectingTime: 1),
                TestData(n: 4, expectingResult: 24, expectingTime: 1),
                TestData(n: 5, expectingResult: 120, expectingTime: 1)
            };
        }

        public static IEnumerable<TestCaseData> TestCaseDatas_BigData(bool withCache)
        {
            if (withCache)
            {
                return new List<TestCaseData>
                {
                    TestData(n: 90, expectingResult: 1, expectingTime: 8),
                };
            }
            return new List<TestCaseData>
            {
                TestData(n: 90, expectingResult: 1, expectingTime: 1),
            };
        }

        public static IEnumerable<TestCaseData> TestCase_SmallDatas()
        {
            return new List<TestCaseData>
            {
                TestData(5),
                TestData(10),
                TestData(15),
                TestData(20)
            };
        }

        public static IEnumerable<TestCaseData> TestCase_BigDatas()
        {
            return new List<TestCaseData>
            {
                TestData(5000),
                TestData(10000),
                TestData(20000),
                TestData(40000)
            };
        }

        public static TestCaseData TestData(int n)
        {
            return new TestCaseData(arg: n);
        }

        public static TestCaseData TestData(int n, BigInteger expectingResult, BigInteger expectingTime)
        {
            return new TestCaseData(arg1: n, arg2: expectingResult, arg3: expectingTime);
        }
    }
}
