﻿using System;
using System.Collections.Generic;
using System.Numerics;
using NUnit.Framework;

namespace Factorials.Common.Tests
{
    /// <summary>
    /// Абстрактный тест, которому не важно какой метод будет считать
    /// </summary>
    public abstract class BaseTests
    {
        public abstract IFactorialMethod GetMethod();

        [Test, TestCaseSource(nameof(SimpleTestCases))]
        public void SuccessTest(int n, BigInteger expectingResult)
        {
            var method = GetMethod();
            var result = method.Calculate(n);

            Assert.That(result, Is.EqualTo(expectingResult));
        }

        [TestCase(-100, true)]
        [TestCase(-1, true)]
        [TestCase(0, false)]
        [TestCase(5, false)]
        public void InvalidArgument_ThrowsException(int n, bool throws)
        {
            var method = GetMethod();

            if (throws)
            {
                Assert.Throws<ArgumentException>(() => method.Calculate(n));
            }
            else
            {
                Assert.DoesNotThrow(() => method.Calculate(n));
            }
        }

        private static IEnumerable<TestCaseData> _testCases;
        public static IEnumerable<TestCaseData> SimpleTestCases
        {
            get
            {
                if (_testCases != null)
                {
                    return _testCases;
                }

                var tempList = new List<TestCaseData>
                {
                    new TestCaseData(0, 1),
                    new TestCaseData(1, 1),
                    new TestCaseData(2, 2),
                    new TestCaseData(3, 6),
                    new TestCaseData(4, 24),
                    new TestCaseData(5, 120)
                };

                return (_testCases = new List<TestCaseData>
                {
                    new TestCaseData(0, (BigInteger)1),
                    new TestCaseData(1, (BigInteger)1),
                    new TestCaseData(2, (BigInteger)2),
                    new TestCaseData(3, (BigInteger)6),
                    new TestCaseData(4, (BigInteger)24),
                    new TestCaseData(5, (BigInteger)120)
                });
            }
        }
    }
}
