﻿using System;
using System.Collections.Generic;
using System.Numerics;
using Factorials.Common.Implementations;
using NUnit.Framework;

namespace Factorials.Common.Tests
{
    /// <summary>
    /// Тесты проверки вычислений
    /// </summary>
    public class RecursiveMethodTests
    {
        [TestCase(-100, true)]
        [TestCase(-1, true)]
        [TestCase(0, false)]
        [TestCase(5, false)]
        public void InvalidArgument_ThrowsException(int n, bool throws)
        {
            var method = GetMethod();

            if (throws)
            {
                Assert.Throws<ArgumentException>(() => method.Calculate(n));
            }
            else
            {
                Assert.DoesNotThrow(() => method.Calculate(n));
            }
        }
        
        [Test, TestCaseSource(nameof(SimpleTestCases))]
        public void SuccessTest(int n, BigInteger expectingResult)
        {
            var method = GetMethod();
            var result = method.Calculate(n);

            Assert.That(result, Is.EqualTo(expectingResult));
        }

        public IFactorialMethod GetMethod()
        {
            return new RecursiveMethod(false);
        }

        private static IEnumerable<TestCaseData> _testCases;
        public static IEnumerable<TestCaseData> SimpleTestCases
        {
            get
            {
                if (_testCases != null)
                {
                    return _testCases;
                }

                return (_testCases = new List<TestCaseData>
                {
                    new TestCaseData(0, 1),
                    new TestCaseData(1, 1),
                    new TestCaseData(2, 2),
                    new TestCaseData(3, 6),
                    new TestCaseData(4, 24),
                    new TestCaseData(5, 120)
                });
            }
        }
    }
}
