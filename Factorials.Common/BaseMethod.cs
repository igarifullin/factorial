﻿using System;
using System.Collections.Generic;

namespace Factorials.Common
{
    public abstract class BaseMethod<TKey, TResult>
    {
        protected readonly Dictionary<TKey, TResult> _cache;
        protected readonly bool _withCache;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="withCache">Флаг использования кеша</param>
        protected BaseMethod(bool withCache)
        {
            _withCache = withCache;
            _cache = new Dictionary<TKey, TResult>();
        }

        /// <summary>
        /// Функция кеширования результата
        /// </summary>
        /// <param name="n">Ключ</param>
        /// <param name="result">Значение</param>
        protected void AddResult(TKey n, TResult result)
        {
            TResult outputResult;
            if (_cache.TryGetValue(key: n, value: out outputResult))
            {
                return;
            }

            _cache.Add(key: n, value: result);
        }

        public TResult Calculate(TKey n)
        {
            if (IsInvalidArgument(n))
            {
                throw new ArgumentException(message: "Parameter 'n' can't be negative");
            }

            TResult result;
            if (_withCache)
            {
                result = CalculateWithCache(n: n);
            }
            else
            {
                result = CalculateWithoutCache(n: n);
            }
            return result;
        }

        /// <summary>
        /// Метод вычисления с использованием кеша
        /// </summary>
        protected abstract TResult CalculateWithCache(TKey n);

        /// <summary>
        /// Метод вычисления без использования кеша
        /// </summary>
        protected abstract TResult CalculateWithoutCache(TKey n);

        /// <summary>
        /// Метод валидация входящего параметра
        /// </summary>
        protected abstract bool IsInvalidArgument(TKey n);
    }
}
