﻿namespace Factorials.UI
{
    partial class FactorialsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CalcButton = new System.Windows.Forms.Button();
            this.NumberLabel = new System.Windows.Forms.Label();
            this.NumberTextBox = new System.Windows.Forms.TextBox();
            this.ResultDataGrid = new System.Windows.Forms.DataGridView();
            this.NumberColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FactorialValueColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NaiveMethodColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecursiveMethodColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SummCombinationColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClearButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ResultDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // CalcButton
            // 
            this.CalcButton.Location = new System.Drawing.Point(87, 35);
            this.CalcButton.Name = "CalcButton";
            this.CalcButton.Size = new System.Drawing.Size(75, 23);
            this.CalcButton.TabIndex = 0;
            this.CalcButton.Text = "Считать";
            this.CalcButton.UseVisualStyleBackColor = true;
            this.CalcButton.Click += new System.EventHandler(this.OnCalcButtonClick);
            // 
            // NumberLabel
            // 
            this.NumberLabel.AutoSize = true;
            this.NumberLabel.Location = new System.Drawing.Point(34, 12);
            this.NumberLabel.Name = "NumberLabel";
            this.NumberLabel.Size = new System.Drawing.Size(48, 13);
            this.NumberLabel.TabIndex = 1;
            this.NumberLabel.Text = "Число n";
            // 
            // NumberTextBox
            // 
            this.NumberTextBox.Location = new System.Drawing.Point(88, 9);
            this.NumberTextBox.Name = "NumberTextBox";
            this.NumberTextBox.Size = new System.Drawing.Size(74, 20);
            this.NumberTextBox.TabIndex = 3;
            // 
            // ResultDataGrid
            // 
            this.ResultDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ResultDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ResultDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NumberColumn,
            this.FactorialValueColumn,
            this.NaiveMethodColumn,
            this.RecursiveMethodColumn,
            this.SummCombinationColumn});
            this.ResultDataGrid.Location = new System.Drawing.Point(184, 9);
            this.ResultDataGrid.Name = "ResultDataGrid";
            this.ResultDataGrid.Size = new System.Drawing.Size(827, 443);
            this.ResultDataGrid.TabIndex = 6;
            // 
            // NumberColumn
            // 
            this.NumberColumn.HeaderText = "Число n";
            this.NumberColumn.Name = "NumberColumn";
            this.NumberColumn.ReadOnly = true;
            this.NumberColumn.Width = 60;
            // 
            // FactorialValueColumn
            // 
            this.FactorialValueColumn.HeaderText = "n!";
            this.FactorialValueColumn.Name = "FactorialValueColumn";
            this.FactorialValueColumn.ReadOnly = true;
            // 
            // NaiveMethodColumn
            // 
            this.NaiveMethodColumn.HeaderText = "Наивный метод";
            this.NaiveMethodColumn.Name = "NaiveMethodColumn";
            this.NaiveMethodColumn.ReadOnly = true;
            this.NaiveMethodColumn.Width = 150;
            // 
            // RecursiveMethodColumn
            // 
            this.RecursiveMethodColumn.HeaderText = "Рекурсивный метод";
            this.RecursiveMethodColumn.Name = "RecursiveMethodColumn";
            this.RecursiveMethodColumn.ReadOnly = true;
            this.RecursiveMethodColumn.Width = 150;
            // 
            // SummCombinationColumn
            // 
            this.SummCombinationColumn.HeaderText = "Метод Амбарцумова Л.Г.";
            this.SummCombinationColumn.Name = "SummCombinationColumn";
            this.SummCombinationColumn.ReadOnly = true;
            this.SummCombinationColumn.Width = 150;
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(8, 35);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(75, 23);
            this.ClearButton.TabIndex = 7;
            this.ClearButton.Text = "Очистить";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.OnClearButtonClick);
            // 
            // FactorialsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 456);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.ResultDataGrid);
            this.Controls.Add(this.NumberTextBox);
            this.Controls.Add(this.NumberLabel);
            this.Controls.Add(this.CalcButton);
            this.Name = "FactorialsForm";
            this.Text = "Вычисление факториала";
            ((System.ComponentModel.ISupportInitialize)(this.ResultDataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CalcButton;
        private System.Windows.Forms.Label NumberLabel;
        private System.Windows.Forms.TextBox NumberTextBox;
        private System.Windows.Forms.DataGridView ResultDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumberColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn FactorialValueColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NaiveMethodColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecursiveMethodColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn SummCombinationColumn;
        private System.Windows.Forms.Button ClearButton;
    }
}

