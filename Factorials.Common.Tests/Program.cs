﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using Factorials.Common.Implementations;
using log4net;

namespace Factorials.Common.Tests
{
    static class Program
    {
        private static ILog _logger;

        public static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            _logger = LogManager.GetLogger(typeof (Program));

            var n = int.Parse(ConfigurationManager.AppSettings["n"]);
            var stackSize = int.Parse(ConfigurationManager.AppSettings["stackSize"]);
            //var stackSize = 1024 * 1024 * 10;

            var thread = new Thread(() =>
            {
                Console.WriteLine("Start to calc Naive Method");
                Calc<NaiveMethod>(n: n, withCache: false);
            }, maxStackSize: stackSize);
            thread.Start();
            thread.Join();
            

            thread = new Thread(() =>
            {
                Console.WriteLine("Start to calc Recursive Method");
                Calc<RecursiveMethod>(n: n, withCache: false);
            }, maxStackSize: stackSize);
            thread.Start();
            thread.Join();

            thread = new Thread(() =>
            {
                Console.WriteLine("Start to calc SummCombination Method");
                Calc<SummCombinationMethod>(n: n, withCache: true);
            }, maxStackSize: stackSize);
            thread.Start();
            thread.Join();

            Console.WriteLine("End of calcalations. Enter the key to exit...");
            Console.ReadKey();
        }

        private static void Calc<T>(int n, bool withCache = false) where T : IFactorialMethod
        {
            _logger.DebugFormat("Start to calculation by method {0}", typeof(T).Name);

            var stopWatch = new Stopwatch();
            var method = GetMethod<T>(withCache: withCache);

            stopWatch.Start();
            var result = method.Calculate(n);
            stopWatch.Stop();

            _logger.DebugFormat("For n = {0}. time = {1}\nResult = {2}", n, stopWatch.ElapsedMilliseconds, result);
        }

        private static IFactorialMethod GetMethod<T>(bool withCache = false) where T : IFactorialMethod
        {
            object[] args = {withCache};
            return (IFactorialMethod) Activator.CreateInstance(typeof(T), args: args);
        }
    }
}
