﻿using System.Numerics;

namespace Factorials.Common
{
    /// <summary>
    /// Интерфейс факториального класса
    /// </summary>
    public interface IFactorialMethod
    {
        /// <summary>
        /// Метод вычисления факториала для заданного числа n
        /// </summary>
        /// <param name="n">Целочисленный параметр, для которого считаем факториал</param>
        /// <returns></returns>
        BigInteger Calculate(int n);
    }
}
