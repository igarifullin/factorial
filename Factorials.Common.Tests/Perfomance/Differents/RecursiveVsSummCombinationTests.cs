﻿using System;
using System.Diagnostics;
using Factorials.Common.Implementations;
using NUnit.Framework;

namespace Factorials.Common.Tests.Perfomance.Differents
{
    public class RecursiveVsSummCombinationTests
    {
        [TestCase(80)]
        public void SuccessTest(int n)
        {
            var recursive = GetRecursiveMethod();
            var summCombination = GetSummCombinationMethod();
            var stopWatch = new Stopwatch();

            stopWatch.Start();
            var recursiveResult = recursive.Calculate(n);
            stopWatch.Stop();
            var recursiveTime = stopWatch.ElapsedMilliseconds;

            Console.WriteLine("[Recursive] result {0}, time {1}", recursiveResult, recursiveTime);

            stopWatch.Reset();
            stopWatch.Start();
            var summCombinationResult = summCombination.Calculate(n);
            stopWatch.Stop();
            var summCombinationTime = stopWatch.ElapsedMilliseconds;

            Console.WriteLine("[SummCombination] result {0}, time {1}", summCombinationResult, summCombinationTime);

            Assert.That(recursiveResult, Is.EqualTo(summCombinationResult));
        }

        public IFactorialMethod GetRecursiveMethod()
        {
            return new RecursiveMethod(withCache: false);
        }

        public IFactorialMethod GetSummCombinationMethod()
        {
            return new SummCombinationMethod(withCache: false);
        }
    }
}
