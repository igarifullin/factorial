﻿using System.Numerics;

namespace Factorials.Common.Implementations
{
    /// <summary>
    /// Класс наивного метода вычисления факториала
    /// </summary>
    public class NaiveMethod : BaseMethod<int,BigInteger>, IFactorialMethod
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="withCache">Флаг использования кеша</param>
        public NaiveMethod(bool withCache) : base(withCache: withCache)
        {
        }

        protected override BigInteger CalculateWithCache(int n)
        {
            BigInteger result = 1;
            for (var i = 2; i <= n; i++)
            {
                if (_cache.TryGetValue(i, out result))
                    continue;

                result *= i;
                AddResult(i, result);
            }

            return result;
        }

        protected override BigInteger CalculateWithoutCache(int n)
        {
            BigInteger result = 1;
            for (var i = 2; i <= n; i++)
            {
                result *= i;
            }

            return result;
        }

        protected override bool IsInvalidArgument(int n)
        {
            return n < 0;
        }
    }
}
