﻿using System.Numerics;

namespace Factorials.Common.Implementations
{
    /// <summary>
    /// Класс классического нахождения факториала
    /// </summary>
    public class RecursiveMethod : BaseMethod<int, BigInteger>, IFactorialMethod
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="withCache">Флаг использования кеша</param>
        public RecursiveMethod(bool withCache) : base(withCache: withCache)
        {
        }

        /// <summary>
        /// Функция вычисления факториала с использованием кеша
        /// </summary>
        protected override BigInteger CalculateWithCache(int n)
        {
            BigInteger result;
            if (_cache.TryGetValue(n, out result))
            {
                return result;
            }

            if (n == 0)
            {
                return 1;
            }

            var previousValue = CalculateWithCache(n - 1);
            AddResult(n: n - 1, result: previousValue);

            return n * previousValue;
        }

        /// <summary>
        /// Функция вычисления факториала без использования кеша
        /// </summary>
        protected override BigInteger CalculateWithoutCache(int n)
        {
            /*if (n > 41000)
            {
                return -1;
            }*/

            if (n == 0)
            {
                return 1;
            }

            return n * CalculateWithoutCache(n - 1);
        }

        protected override bool IsInvalidArgument(int n)
        {
            return n < 0;
        }
    }
}
