﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Numerics;
using System.Threading;
using System.Windows.Forms;
using Factorials.Common;
using Factorials.Common.Implementations;
using log4net;

namespace Factorials.UI
{
    public partial class FactorialsForm : Form
    {
        private readonly ILog _logger;

        public FactorialsForm()
        {
            InitializeComponent();
            log4net.Config.XmlConfigurator.Configure();
            _logger = LogManager.GetLogger(typeof(Program));
        }

        private void OnCalcButtonClick(object sender, EventArgs e)
        {
            //var n = int.Parse(ConfigurationManager.AppSettings["n"]);

            if (string.IsNullOrEmpty(NumberTextBox.Text.Trim()))
            {
                MessageBox.Show("Число n не задано. Для расчетов задайте целочисленное n", "Не задан входной параметр", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                return;
            }

            int n;
            if (!int.TryParse(NumberTextBox.Text, out n))
            {
                MessageBox.Show("Число n не задано. Для расчетов задайте целочисленное n", "Не задан входной параметр", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                return;
            }

            var stackSize = int.Parse(ConfigurationManager.AppSettings["stackSize"]);

            var dictionaryResult = new Dictionary<Type, CalculationResult>();
                 
            var thread = new Thread(() =>
            {
                _logger.Info("Start to calc Naive Method");
                var result = Calc<NaiveMethod>(n: n, withCache: false);
                dictionaryResult.Add(result.Key, result.Value);
            }, maxStackSize: stackSize);
            thread.Start();
            thread.Join();


            thread = new Thread(() =>
            {
                _logger.Info("Start to calc Recursive Method");
                var result = Calc<RecursiveMethod>(n: n, withCache: false);
                dictionaryResult.Add(result.Key, result.Value);
            }, maxStackSize: stackSize);
            thread.Start();
            thread.Join();

            thread = new Thread(() =>
            {
                _logger.Info("Start to calc SummCombination Method");
                var result = Calc<SummCombinationMethod>(n: n, withCache: true);
                dictionaryResult.Add(result.Key, result.Value);
            }, maxStackSize: stackSize);
            thread.Start();
            thread.Join();

            var factorialValue = dictionaryResult[typeof(NaiveMethod)].Result;

            ResultDataGrid.Rows.Add(n, factorialValue, dictionaryResult[typeof(NaiveMethod)].Time,
                dictionaryResult[typeof(RecursiveMethod)].Time, dictionaryResult[typeof(SummCombinationMethod)].Time);

            _logger.Info("End of calculations.");
        }

        private KeyValuePair<Type, CalculationResult> Calc<T>(int n, bool withCache = false) where T : IFactorialMethod
        {
            _logger.DebugFormat("Start to calculation by method {0}", typeof(T).Name);

            var stopWatch = new Stopwatch();
            var method = GetMethod<T>(withCache: withCache);

            stopWatch.Start();

            BigInteger result = 0;
            try
            {
                result = method.Calculate(n);
            }
            catch(Exception)
            {
                result = -1;
            }
            stopWatch.Stop();

            _logger.DebugFormat("For n = {0}. time = {1}\nResult = {2}", n, stopWatch.ElapsedMilliseconds, result);

            var calculationResult = new CalculationResult(stopWatch.ElapsedMilliseconds, result);

            return new KeyValuePair<Type, CalculationResult>(typeof(T), calculationResult);
        }

        private static IFactorialMethod GetMethod<T>(bool withCache = false) where T : IFactorialMethod
        {
            object[] args = { withCache };
            return (IFactorialMethod)Activator.CreateInstance(typeof(T), args: args);
        }

        private void OnClearButtonClick(object sender, EventArgs e)
        {
            ResultDataGrid.Rows.Clear();
        }
    }

    public class CalculationResult
    {
        public long Time { get; set; }

        public BigInteger Result { get; set; }

        public CalculationResult(long time, BigInteger result)
        {
            Time = time;
            Result = result;
        }
    }
}
